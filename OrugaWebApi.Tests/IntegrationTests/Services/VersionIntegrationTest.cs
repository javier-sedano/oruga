using System;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using OrugaWebApi.ServicesInterfaces;
using Xunit;

namespace OrugaWebApi.IntegrationTests.Services
{
    public class VersionIntegrationTest
        : IClassFixture<WebApplicationFactory<OrugaWebApi.Startup>>
    {
        private readonly IServiceScope _scope;
        private readonly IVersionService _versionService;
        public VersionIntegrationTest(WebApplicationFactory<OrugaWebApi.Startup> factory)
        {
            _scope = factory.Services.CreateScope();
            _versionService = _scope.ServiceProvider.GetRequiredService<IVersionService>();
        }

        ~VersionIntegrationTest()
        {
            _scope.Dispose();
        }

        [Fact]
        public void GetVersion()
        {
            string version = _versionService.Version;
            Console.WriteLine("V: " + version);
            Assert.True(version.Length > 0);
        }

    }
}