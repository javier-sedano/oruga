using System;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using OrugaWebApi.ServicesInterfaces;
using Xunit;

namespace OrugaWebApi.IntegrationTests.Services
{
    public class FortuneIntegrationTest
        : IClassFixture<WebApplicationFactory<OrugaWebApi.Startup>>
    {
        private readonly IServiceScope _scope;
        private readonly IFortuneService _fortuneService;
        public FortuneIntegrationTest(WebApplicationFactory<OrugaWebApi.Startup> factory)
        {
            _scope = factory.Services.CreateScope();
            _fortuneService = _scope.ServiceProvider.GetRequiredService<IFortuneService>();
        }

        ~FortuneIntegrationTest()
        {
            _scope.Dispose();
        }

        [Fact]
        public void GetOneFortune()
        {
            string[] fortunes = _fortuneService.GetFortunes(1);
            Console.WriteLine("One fortune: " + string.Join("\n\n", fortunes));
            Assert.Single(fortunes);
        }

        [Fact]
        public void GetSeveralFortunes()
        {
            string[] fortunes = _fortuneService.GetFortunes(TestConstants.MaxFortunes);
            Assert.Equal(TestConstants.MaxFortunes, fortunes.Length);
        }

        [Fact]
        public void GetTooManyFortunes()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _fortuneService.GetFortunes(TestConstants.MaxFortunes + 1));
        }

        [Fact]
        public void GetTooFewFortunes()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _fortuneService.GetFortunes(0));
        }

    }
}