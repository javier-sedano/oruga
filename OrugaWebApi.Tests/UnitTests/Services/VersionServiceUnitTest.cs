using Xunit;
using OrugaWebApi.Services;
using Moq;
using Microsoft.Extensions.Logging;

namespace OrugaWebApi.UnitTests.Services
{
    public class VersionServiceUnitTest
    {
        [Fact]
        public void GetVersion()
        {
            Mock<ILogger<VersionService>> mockLogger = new Mock<ILogger<VersionService>>();
            VersionService versionService = new VersionService(mockLogger.Object);
            string version = versionService.Version;
            Assert.True(version.Length > 0);
        }
    }
}