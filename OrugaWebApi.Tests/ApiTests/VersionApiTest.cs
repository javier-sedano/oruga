using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace OrugaWebApi.ApiTests
{
    public class VersionApiTest
        : IClassFixture<WebApplicationFactory<OrugaWebApi.Startup>>
    {
        private const string URL = ApiTestConstants.RestUrl + "version";
        private readonly WebApplicationFactory<OrugaWebApi.Startup> _factory;

        public VersionApiTest(WebApplicationFactory<OrugaWebApi.Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GetVersion()
        {
            HttpClient client = _factory.CreateClient();
            HttpResponseMessage response = await client.GetAsync(URL);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            string content = await response.Content.ReadAsStringAsync();
            Console.WriteLine("Version: " + content);
            Assert.True(content.Length > 0);
        }
    }
}