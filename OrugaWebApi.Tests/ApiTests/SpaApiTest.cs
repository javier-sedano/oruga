using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace OrugaWebApi.ApiTests
{
    public class SpaApiTest
        : IClassFixture<SpaApiSpaApiTestCustomApplicationFactoryTest<OrugaWebApi.Startup>>
    {
        private readonly SpaApiSpaApiTestCustomApplicationFactoryTest<OrugaWebApi.Startup> _factory;

        public SpaApiTest(SpaApiSpaApiTestCustomApplicationFactoryTest<OrugaWebApi.Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task WebReturnsIndex()
        {
            await _UrlReturnsSpaIndex(ApiTestConstants.WebUrl);
        }

        [Fact]
        public async Task WebIndexReturnsIndex()
        {
            await _UrlReturnsSpaIndex(ApiTestConstants.WebUrl + "index.html");
        }

        [Fact]
        public async Task WebNonExistingFileNotFound()
        {
            await _UrlNotFound(ApiTestConstants.WebUrl + "nonExistingResource.txt");
        }

        [Fact]
        public async Task WebNonExistingReturnsIndex()
        {
            await _UrlReturnsSpaIndex(ApiTestConstants.WebUrl + "nonExistingResource");
        }

        [Fact]
        public async Task WebDeepNonExistingReturnsIndex()
        {
            await _UrlReturnsSpaIndex(ApiTestConstants.WebUrl + "non/existing/resource");
        }

        [Fact]
        public async Task WebExistingReturnsResource()
        {
            await _UrlReturnsContent(ApiTestConstants.WebUrl + "webResource.txt", "webResource.txt");
        }

        [Fact]
        public async Task NonWebNonExistingNotFound()
        {
            await _UrlNotFound(ApiTestConstants.BaseUrl + "nonExistingResource.txt");
        }

        [Fact]
        public async Task NonWebExistingReturnsResource()
        {
            await _UrlReturnsContent(ApiTestConstants.BaseUrl + "nonWebResource.txt", "nonWebResource.txt");
        }

        private async Task _UrlNotFound(string url)
        {
            HttpClient client = _factory.CreateClient();
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        private async Task _UrlReturnsContent(string url, string expectedContent)
        {
            HttpClient client = _factory.CreateClient();
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            string content = await response.Content.ReadAsStringAsync();
            Assert.Equal(expectedContent, content);

        }
        private async Task _UrlReturnsSpaIndex(string url)
        {
            await _UrlReturnsContent(url, "index.html");
        }


    }
}