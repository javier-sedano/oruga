using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;

namespace OrugaWebApi.ApiTests
{
    public class SpaApiSpaApiTestCustomApplicationFactoryTest<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.UseWebRoot("../OrugaWebApi.Tests/wwwroot/");
        }
    }
}