using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace OrugaWebApi.ApiTests
{
    public class RootApiTest
        : IClassFixture<WebApplicationFactory<OrugaWebApi.Startup>>
    {
        private readonly WebApplicationFactory<OrugaWebApi.Startup> _factory;

        public RootApiTest(WebApplicationFactory<OrugaWebApi.Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task RootRedirectToBase()
        {
            await _Redirect(ApiTestConstants.RootUrl, ApiTestConstants.BaseUrl);
        }

        [Fact]
        public async Task BaseRedirectToIndex()
        {
            await _Redirect(ApiTestConstants.BaseUrl, ApiTestConstants.WebUrl);
        }

        private async Task _Redirect(string url, string expectedLocation)
        {
            WebApplicationFactoryClientOptions clientOptions = new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            };
            HttpClient client = _factory.CreateClient(clientOptions);
            HttpResponseMessage response = await client.GetAsync(url);
            Assert.Equal(HttpStatusCode.Redirect, response.StatusCode);
            string location = response.Headers.Location.ToString();
            Console.WriteLine("location: " + location);
            Assert.Equal(expectedLocation, location);
        }

    }
}