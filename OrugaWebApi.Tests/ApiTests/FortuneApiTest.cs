using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json.Linq;
using Xunit;

namespace OrugaWebApi.ApiTests
{
    public class FortuneApiTest
        : IClassFixture<WebApplicationFactory<OrugaWebApi.Startup>>
    {
        private const string URL = ApiTestConstants.RestUrl + "fortune";
        private readonly WebApplicationFactory<OrugaWebApi.Startup> _factory;

        public FortuneApiTest(WebApplicationFactory<OrugaWebApi.Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GetFortune()
        {
            HttpClient client = _factory.CreateClient();
            HttpResponseMessage response = await client.GetAsync(URL);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("application/json", response.Content.Headers.ContentType.MediaType);
            string content = await response.Content.ReadAsStringAsync();
            JArray fortunes = JArray.Parse(content);
            Assert.Single(fortunes);
            Assert.True(fortunes[0].Value<string>().Length > 0);
        }

        [Fact]
        public async Task GetOneFortune()
        {
            HttpClient client = _factory.CreateClient();
            HttpResponseMessage response = await client.GetAsync(URL + "?n=1");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("application/json", response.Content.Headers.ContentType.MediaType);
            string content = await response.Content.ReadAsStringAsync();
            JArray fortunes = JArray.Parse(content);
            Assert.Single(fortunes);
            Assert.True(fortunes[0].Value<string>().Length > 0);
        }

        [Fact]
        public async Task GetSeveralFortunes()
        {
            HttpClient client = _factory.CreateClient();
            HttpResponseMessage response = await client.GetAsync(URL + "?n=" + TestConstants.MaxFortunes);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("application/json", response.Content.Headers.ContentType.MediaType);
            string content = await response.Content.ReadAsStringAsync();
            Console.WriteLine(content);
            JArray fortunes = JArray.Parse(content);
            Assert.Equal(TestConstants.MaxFortunes, fortunes.Count);
            Assert.True(fortunes[0].Value<string>().Length > 0);
        }

        [Fact]
        public async Task GetTooManyFortunes()
        {
            HttpClient client = _factory.CreateClient();
            HttpResponseMessage response = await client.GetAsync(URL + "?n=" + (TestConstants.MaxFortunes + 1));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task GetTooFewFortunes()
        {
            HttpClient client = _factory.CreateClient();
            HttpResponseMessage response = await client.GetAsync(URL + "?n=0");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

    }
}