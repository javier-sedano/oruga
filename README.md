Reference architecture of public web application with mobile-first responsive design based on C# ASP.NET Core and Vue with CI in Gitlab-CI and CD to Google Compute Engine.

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/oruga/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/oruga/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=coverage)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.oruga&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.oruga)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.vaca)
</details>

Keywords: C#, .NET Core, ASP.NET, xunit, sonarqube, vue, jest, vscode, devcontainer, docker, nodemon, w3css, fontawesome, giphy

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Open VSCode and use `F1` --> `Remote containers: Clone Repository in Container Volume...` --> Paste Git clone URL --> `Create a new volume...` (or choose an existing one) --> Accept default name
  * Remote container will be detected and automatically used.
* Makefile targets will be shown in Task Explorer
  * Run "dependenciesInit" to download dependencies (command line: make dependenciesInit)
    * You may need to restart the Omnisharp plugin for it to find the dependencies: use the VSCode command palete (Ctrl-Shift-P) and `OmniSharp: Restart OmniSharp`
  * Run "devStart" to start serving (while watching for changes in both frontend and backend) (command line: make devStart)
    * Most common development tasks are configured as Makefile targets
  * Browse http://localhost:4005/ (if dockerized dev env) of http://localhost:5000/ (if non-dockerized)
* VSCode shows the most useful tools, like Solution Explorer, Tests,...

Useful links:

* https://code.visualstudio.com/docs/remote/containers
* https://code.visualstudio.com/docs/editor/tasks
* https://www.gnu.org/software/make/manual/make.html
* https://docs.microsoft.com/en-us/dotnet/core/
* https://docs.microsoft.com/en-us/aspnet/core/introduction-to-aspnet-core?view=aspnetcore-3.1
* https://docs.microsoft.com/en-us/dotnet/api/?view=netcore-3.1
* https://docs.microsoft.com/en-us/dotnet/api/?view=aspnetcore-3.1
* https://www.nuget.org/
* https://github.com/RetireNet/dotnet-retire
* https://xunit.net/
* https://github.com/Moq/moq4/wiki/Quickstart
* https://vuejs.org/
* https://cli.vuejs.org/
* https://router.vuejs.org/
* https://jestjs.io/
* https://github.com/axios/axios
* https://www.w3schools.com/w3css/
* https://fontawesome.com/
* https://developers.giphy.com/docs/api/endpoint/#random
* https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-msbuild/
* https://github.com/tonerdo/coverlet
* https://www.newtonsoft.com/json
