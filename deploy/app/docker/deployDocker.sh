#!/bin/bash
cd "$(dirname "$0")"
TAG=latest
if [ "$1" ]
then
  TAG="$1"
fi
REGISTRY=registry.gitlab.com/javier-sedano/oruga/app
IMAGE=$REGISTRY:$TAG
containerId=`docker ps --filter "name=oruga" --filter "status=running" --quiet`
docker container stop oruga
docker container rm oruga
docker image ls | grep $REGISTRY | while read LINE
do
  OLD_TAG=`echo $LINE | cut -d\  -f2`
  docker image rm $REGISTRY:$OLD_TAG
done
docker pull $IMAGE
docker container create -m 32m -p 5580:80 --restart unless-stopped --name oruga $IMAGE
if [ "$containerId" ]
then
  docker container start oruga
fi

