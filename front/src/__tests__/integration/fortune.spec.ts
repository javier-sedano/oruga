import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import { mount, flushPromises, VueWrapper } from "@vue/test-utils";
import { setActivePinia, createPinia } from "pinia";
import { REST_BASE_URL, mockAxios } from "../tests.utils";
import Fortune from "../../views/Fortune.vue";
import { useErrorStore } from "../../stores/error.store";

const { axiosGetMock } = mockAxios();

const REST_FORTUNE_URL = REST_BASE_URL + "/fortune";

beforeEach(() => {
  setActivePinia(createPinia());
});

afterEach(() => {
  vi.clearAllMocks();
});

describe("Fortune view", async () => {
  async function createWrapper(): Promise<VueWrapper> {
    const aFortune = "A fortune cookie";
    axiosGetMock.mockImplementationOnce(() =>
      Promise.resolve({
        data: [
          aFortune,
        ],
      }),
    );
    const wrapper = mount(Fortune, {
      global: {
        stubs: {
          "font-awesome-icon": { template: "<span font-awesome-icon/>" },
        },
      },
      attachTo: document.body,
    });
    expect(wrapper.find('[data-test-id~="loading"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="fortuneContainer"]').exists()).toEqual(false);
    expect(wrapper.element).toMatchSnapshot("Initial");
    await flushPromises();
    expect(wrapper.find('[data-test-id~="loading"]').exists()).toEqual(false);
    expect(wrapper.find('[data-test-id~="fortuneContainer"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="fortuneText"]').text()).toEqual(aFortune);
    expect(axiosGetMock).toHaveBeenCalledWith(REST_FORTUNE_URL, { params: { n: 1 } });
    axiosGetMock.mockClear();
    expect(wrapper.element).toMatchSnapshot("Initial with fortune");
    return wrapper;
  }
  it("Should create with a fortune", async () => {
    await createWrapper();
  });
  it("Should create with a fortune and reload", async () => {
    const wrapper = await createWrapper();
    const anotherFortune = "Another fortune cookie";
    axiosGetMock.mockImplementationOnce(() =>
      Promise.resolve({
        data: [
          anotherFortune,
        ],
      }),
    );
    await wrapper.find('[data-test-id~="fortuneContainer"]').trigger("click");
    expect(wrapper.find('[data-test-id~="fortuneText"]').text()).toEqual(anotherFortune);
    expect(axiosGetMock).toHaveBeenCalledWith(REST_FORTUNE_URL, { params: { n: 1 } });
    expect(wrapper.element).toMatchSnapshot("With another fortune");
  });
  it("Should create with a fortune and detect fail on reload", async () => {
    const wrapper = await createWrapper();
    const anError = "An error";
    axiosGetMock.mockImplementationOnce(() => Promise.reject(new Error(anError)));
    await wrapper.find('[data-test-id~="fortuneContainer"]').trigger("click");
    expect(axiosGetMock).toHaveBeenCalledWith(REST_FORTUNE_URL, { params: { n: 1 } });
    expect(wrapper.find('[data-test-id~="loading"]').exists()).toEqual(true);
    expect(wrapper.find('[data-test-id~="fortuneContainer"]').exists()).toEqual(false);
    expect(useErrorStore().errorMessage).toEqual(`Error: ${anError}`);
    expect(wrapper.element).toMatchSnapshot("With error");
  });
});
