import { describe, it, expect } from "vitest";
import { mockAxios } from "../../tests.utils";
import { oracleService } from "../../../services/oracle.service";

const { axiosGetMock } = mockAxios();

const GIPHY_URL = "https://api.giphy.com/v1/gifs/random";
const MAX_RELATION_FACTOR = 3;

describe("oracle service", async () => {
  it("Should retry ratios too large or too small", async () => {
    global.Math.random = () => 0;
    const tooWideGif = {
      data: {
        images: {
          original: {
            height: 10,
            width: 10 * (MAX_RELATION_FACTOR * 1.1),
            url: "https://an/urlWide.gif",
          },
        },
        url: "https://giphy/url",
        user: {
          display_name: "qwerty",
        },
      },
    };
    const tooHighGif = {
      data: {
        images: {
          original: {
            height: 10 * (MAX_RELATION_FACTOR * 1.1),
            width: 10,
            url: "https://an/urlHigh.gif",
          },
        },
        url: "https://giphy/url",
        user: {
          display_name: "qwerty",
        },
      },
    };
    const correctRatioGif = {
      data: {
        images: {
          original: {
            height: 10,
            width: 10 * (MAX_RELATION_FACTOR * 0.9),
            url: "https://an/url.gif",
          },
        },
        url: "https://giphy/url",
        user: {
          display_name: "qwerty",
        },
      },
    };
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: tooWideGif }));
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: tooHighGif }));
    axiosGetMock.mockImplementationOnce(() => Promise.resolve({ data: correctRatioGif }));
    const numberOfwrongRatioGifs = 2;
    const numberOfGifs = numberOfwrongRatioGifs + 1;
    const respose = await oracleService.getResponse(1);
    expect(axiosGetMock).toHaveBeenCalledTimes(numberOfGifs);
    expect(respose).toEqual({
      answer: "Yes",
      author: correctRatioGif.data.user.display_name,
      gifUrl: correctRatioGif.data.images.original.url,
      giphyUrl: correctRatioGif.data.url,
    });
  });
});
