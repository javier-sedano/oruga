import axios from "axios";
import constants from "./service.constants";

const url = constants.restBaseUrl + "/version";
export const versionService = {
  getVersion: async (): Promise<string> => {
    const response = await axios.get<string>(url);
    return response.data;
  },
};
