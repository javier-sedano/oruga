import axios from "axios";
import constants from "./service.constants";

const url = constants.restBaseUrl + "/fortune";
export const fortuneService = {
  getFortune: async (): Promise<string> => {
    return (await axios.get(url, { params: { n: 1 } })).data[0];
  },
};
