namespace OrugaWebApi.ServicesInterfaces
{
    public interface IVersionService
    {
        string Version { get; }
    }
}
