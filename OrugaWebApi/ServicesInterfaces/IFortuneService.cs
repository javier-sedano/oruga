namespace OrugaWebApi.ServicesInterfaces
{
    public interface IFortuneService
    {
        string[] GetFortunes(int n);
    }
}
