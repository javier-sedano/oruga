using Microsoft.Extensions.Logging;
using OrugaWebApi.ServicesInterfaces;

namespace OrugaWebApi.Services
{
    public class VersionService : IVersionService
    {
        private readonly ILogger<VersionService> _logger;

        public VersionService(ILogger<VersionService> logger)
        {
            _logger = logger;
        }

        public string Version
        {
            get
            {
                string version = GetType().Assembly.GetName().Version.ToString();
                _logger.LogDebug("Assembly version is: {Version}", version);
                return version;
            }
        }

    }
}
