using System;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using OrugaWebApi.ServicesInterfaces;

namespace OrugaWebApi.Services
{
    public class FortuneService : IFortuneService
    {
        private readonly ILogger<FortuneService> _logger;
        private const int MAX_FORTUNES = 5;
        private const string FORTUNE = "/usr/games/fortune";

        public FortuneService(ILogger<FortuneService> logger)
        {
            _logger = logger;
        }

        public string[] GetFortunes(int n)
        {
            _logger.LogDebug("Generating {N} fortunes", n);
            ArgumentOutOfRangeException.ThrowIfNegativeOrZero(n);
            ArgumentOutOfRangeException.ThrowIfGreaterThan(n, MAX_FORTUNES);
            string[] fortunes = new string[n];
            for (int i = 0; i < n; i++)
            {
                string fortune = _Bash(FORTUNE);
                fortunes[i] = fortune;
            }
            return fortunes;
        }

        private static string _Bash(string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            return result;
        }

    }
}
