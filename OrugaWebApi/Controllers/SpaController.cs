﻿using System.Net.Mime;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace OrugaWebApi.Controllers
{
    [Controller]
    [Route(WebConstants.WebUrl)]
    public class SpaController : ControllerBase
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public SpaController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        [Route("index.html")]
        public IStatusCodeActionResult Index()
        {
            return new ContentResult()
            {
                StatusCode = StatusCodes.Status200OK,
                ContentType = MediaTypeNames.Text.Html,
                Content = System.IO.File.ReadAllText(_webHostEnvironment.WebRootPath + WebConstants.WebUrl + "index.html")
            };
        }

    }
}
