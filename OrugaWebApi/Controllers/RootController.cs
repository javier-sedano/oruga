﻿using Microsoft.AspNetCore.Mvc;

namespace OrugaWebApi.Controllers
{
    [ApiController]
    [Route(WebConstants.RootUrl)]
    public class RootController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetRoot()
        {
            return Redirect(WebConstants.BaseUrl);
        }

        [HttpGet(WebConstants.BaseUrl)]
        public IActionResult GetApp()
        {
            return Redirect(WebConstants.WebUrl);
        }
    }
}
