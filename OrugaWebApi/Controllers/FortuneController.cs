﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrugaWebApi.ServicesInterfaces;

namespace OrugaWebApi.Controllers
{
    [ApiController]
    [Route(WebConstants.RestUrl + "fortune")]
    public class FortunesController : ControllerBase
    {
        private readonly ILogger<FortunesController> _logger;
        private readonly IFortuneService _fortuneService;

        public FortunesController(ILogger<FortunesController> logger, IFortuneService fortuneService)
        {
            _logger = logger;
            _fortuneService = fortuneService;
        }

        [HttpGet]
        public string[] Get([FromQuery(Name = "n")] int n = 1)
        {
            try
            {
                return _fortuneService.GetFortunes(n);
            }
            catch (ArgumentOutOfRangeException e)
            {
                _logger.LogDebug(e, "{Message}", e.Message);
                throw new HttpResponseException(StatusCodes.Status400BadRequest, e.Message);
            }
        }
    }
}

