﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrugaWebApi.ServicesInterfaces;

namespace OrugaWebApi.Controllers
{
    [ApiController]
    [Route(WebConstants.RestUrl + "version")]
    public class VersionController : ControllerBase
    {
        private readonly ILogger<VersionController> _logger;
        private readonly IVersionService _versionService;

        public VersionController(ILogger<VersionController> logger, IVersionService versionService)
        {
            _logger = logger;
            _versionService = versionService;
        }

        [HttpGet]
        public string Get()
        {
            string version = _versionService.Version;
            _logger.LogDebug("Returning version: {Version}", version);
            return version;
        }
    }
}
