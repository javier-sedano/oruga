using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace OrugaWebApi.Controllers
{

#pragma warning disable S3925
    [Serializable]
    public class HttpResponseException : Exception
    {
        public int Status { get; set; }

        public object Value { get; set; }

        public HttpResponseException(int status, object value)
        {
            Status = status;
            Value = value;
        }
    }
#pragma warning restore S3925
}