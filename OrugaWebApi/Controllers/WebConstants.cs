﻿namespace OrugaWebApi.Controllers
{
    public static class WebConstants
    {
        public const string RootUrl = "/";
        public const string App = "oruga/";
        public const string BaseUrl = RootUrl + App;
        public const string RestUrl = BaseUrl + "rest/";
        public const string WebUrl = BaseUrl + "web/";
    }
}
