ifeq ($(VERSION),)
	VERSION := 0.0.0-dev
endif
PVERSION=/p:Version=$(VERSION)

dependenciesInit:
	dotnet restore
	dotnet tool restore
	cd front && npm run dependenciesInit && cd ..

devStart:
	./parallel.sh "make buildFrontWatch" "make dotnetStart"

dotnetStart:
	dotnet watch --project OrugaWebApi run --urls http://0.0.0.0:5000

clean:
	rm -rf OrugaWebApi/bin/
	rm -rf OrugaWebApi/obj/
	rm -rf OrugaWebApi.Tests/bin/
	rm -rf OrugaWebApi.Tests/obj/
	rm -f OrugaWebApi.Tests/coverage.opencover.xml
	rm -rf published/
	rm -rf .sonarqube/
	rm -rf OrugaWebApi/wwwroot/oruga/web
	cd front && npm run clean && cd ..

deepClean: clean
	rm -rf front/node_modules

build: buildFront
	dotnet publish -c Release -o published $(PVERSION)

buildFront:
	cd front && npm run build-only -- --outDir=../OrugaWebApi/wwwroot/oruga/web && cd ..

buildFrontWatch:
	cd front && npm run build-only -- --mode=development --watch --emptyOutDir --sourcemap=true --minify=false --outDir=../OrugaWebApi/wwwroot/oruga/web && cd ..

buildForDebug: buildFront
	dotnet build OrugaWebApi/OrugaWebApi.csproj /property:GenerateFullPaths=true /consoleloggerparameters:NoSummary

-include .sonar_token.mk
sonar:
	dotnet sonarscanner begin \
		/k:com.odroid.oruga /n:oruga \
		/d:sonar.sources=front/src \
		/d:sonar.cs.opencover.reportsPaths=OrugaWebApi.Tests/coverage.opencover.xml \
		/d:sonar.javascript.lcov.reportPaths=front/coverage/lcov.info \
		/d:sonar.exclusions=OrugaWebApi/Program.cs,OrugaWebApi/Startup.cs,front/src/main.ts,front/public/css/w3.css \
		/d:sonar.coverage.exclusions=OrugaWebApi/Program.cs,OrugaWebApi/Startup.cs,front/src/main.ts,front/src/router/index.ts,front/**/*.spec.ts,front/*.js \
		/v:0.0.0 /d:sonar.host.url=http://sonarqube:9000 \
		/d:sonar.token=$(SONAR_TOKEN)
	dotnet build
	dotnet sonarscanner end \
		/d:sonar.token=$(SONAR_TOKEN)
	@echo "If dockerized, browse http://localhost:9005"

sonarqube-start:
	./.devcontainer/sonarqube_start.sh

sonarqube-stop:
	./.devcontainer/sonarqube_stop.sh

testWatch:
	dotnet watch --project OrugaWebApi.Tests/ test

testWatchFilter:
	dotnet watch --project OrugaWebApi.Tests/ test --filter Version

test:
	dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover

testFront:
	cd front && npm run test -- run && cd ..

testFrontWatch:
	cd front && npm run test && cd ..

test-n-sonar:
	make test
	make testFront
	make sonarqube-start
	make sonar

audit: audit-dotnet audit-front

audit-withIgnores: audit-dotnet audit-frontWithIgnores

audit-dotnet:
	cd OrugaWebApi && dotnet tool run dotnet-retire; cd ..

audit-front:
	cd front && npm audit && cd ..

audit-frontWithIgnores:
	cd front && npm run auditWithIgnores && cd ..
