#!/bin/bash -e
pushd `dirname $0` > /dev/null

docker-compose -p oruga_devcontainer stop sonarqube

popd > /dev/null
