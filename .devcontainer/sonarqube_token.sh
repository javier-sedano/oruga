#!/bin/bash
pushd `dirname $0` > /dev/null

curl -s -u admin:admin1 -X POST http://sonarqube:9000/api/user_tokens/revoke?name=autotoken
curl -s -u admin:admin1 -X POST http://sonarqube:9000/api/user_tokens/generate?name=autotoken | jq -r ".token"

popd > /dev/null
